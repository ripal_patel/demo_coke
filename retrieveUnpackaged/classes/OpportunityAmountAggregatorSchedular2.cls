global class OpportunityAmountAggregatorSchedular2 implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        OppAmountAgg op=new OppAmountAgg();
        Database.executeBatch(op);
    }
}