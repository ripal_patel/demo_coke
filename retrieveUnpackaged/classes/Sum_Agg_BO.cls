/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */

  @isTest
public with sharing class Sum_Agg_BO{
 static testmethod void TestAggregate(){
  
  //Integer sumAmount = 0;
  
  //String qry = 'Select o.StageName, o.Name, o.Amount, o.IsAggregated__c From Opportunity o Where o.StageName = \'Closed Won\'';
  List<Opportunity>lstOpp = new List<Opportunity>();
  
  for(Integer i=0;i<199;i++){
   Opportunity newOpp = new Opportunity();
   newOpp.Name = 'Test Opportunity';
   newOpp.CloseDate = Date.today();
   newOpp.StageName = 'Closed Won';
   newOpp.Amount = 100;
     
   lstOpp.add(newOpp);
  }
  
  insert  lstOpp;
  
  Test.startTest();
  
 OppAmountAgg oppAgg = new OppAmountAgg();
  Database.executeBatch(oppAgg);
  
  Test.stopTest();
  
  List<Opportunity> lstFinal = [SELECT o.Name, o.Id FROM Opportunity o WHERE o.isAggregated__c = true];
  
  system.assertEquals(199, lstFinal.size());
  
 }
}