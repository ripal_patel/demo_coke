public with sharing class Taskhandlerclass {

 public void task(List<Account> acc)
 {
   
  List<Task> task = new List<Task>();
 
 
  for(Account a:acc)
  {
   Task t = new Task();
   t.Subject='Meeting with '+a.Name;
   t.Priority='high';
   t.WhatId=a.Id;
   
   task.add(t);  
  }
 
  insert task;
 
  System.debug('Task created successfully');
 }
}