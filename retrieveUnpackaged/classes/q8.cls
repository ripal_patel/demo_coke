public with sharing class q8 {

public void q1()
{
 Account a= new Account(Name='ripal23');
 insert a;
}
public void q2()
{
 Contact c= new Contact(Lastname='ripal23');
 insert c;
}

public void q3()
{
 Account a= new Account(Name='ripal12255183');
 insert a;
 Contact c= new Contact(Lastname='ripal125289583');
 c.AccountId=a.Id;
 insert c;
 c = [select account.name from contact where id = :c.id];
 c.Account.Name='Rutuja_Bhandari';
 c.Lastname='Ripal_Patel';
 update c;
 update c.Account;
 
}
}