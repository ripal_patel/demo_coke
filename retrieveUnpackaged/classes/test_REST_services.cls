/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class test_REST_services {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
       
        
     RestRequest req = new RestRequest(); 
  RestResponse res = new RestResponse();
 
  req.requestURI = 'https://ap1.salesforce.com/services/apexrest/Lead';  
  req.httpMethod = 'POST';
  RestContext.request = req;
  RestContext.response = res;
  
  REST_Service_Lead.wrapper results = REST_Service_Lead.doPost('N1','N2','n1@n2.com','34128967' );
  try{
  System.assertEquals(true, results.isSuccess);
  System.assertEquals('Success', results.status);
  }catch(Exception e){
   System.assertEquals(results.isSuccess, false);
   System.assertEquals(results.status, e.getMessage());
  }
  REST_Service_Lead.wrapper result = REST_Service_Lead.doPut('N1','N2','n1@n2.com','34128967' );
  try{
  System.assertEquals(true, result.isSuccess);
  System.assertEquals('Success', result.status);
  }catch(Exception e){
   System.assertEquals(result.isSuccess, false);
   System.assertEquals(result.status, e.getMessage());
  }

        req.requestURI = 'https://ap1.salesforce.com/services/apexrest/Lead'; 
        req.addParameter('fName', 'N1');
        req.addParameter('lName', 'N2');
        req.addParameter('email', 'n1@n2.com');
        req.addParameter('phone', '34128967');
  
        req.httpMethod = 'Delete';
        REST_Service_Lead.wrapper result1 = REST_Service_Lead.doDelete();
  try{
  System.assertEquals(true, result1.isSuccess);
  System.assertEquals('Success', result1.status);
  }catch(Exception e){
   System.assertEquals(result1.isSuccess, false);
   System.assertEquals(result1.status, e.getMessage());
  }
    }
}