public with sharing class oceanPropertiesExtension {

    private Account accRecord;
    
    public class imageWrapper{
        
        public Id imageID {get;set;}
        public boolean isChecked {get;set;}
        public String imageName {get;set;}
        
        public imageWrapper(Id imgID, String imgName){
            imageID = imgID;
            imageName = imgName;
            isChecked = false;
        }    
    }
    
    public List<imageWrapper>imgWrapper {get;set;}
    
    public oceanPropertiesExtension(ApexPages.StandardController controller){
        this.accRecord = (Account)controller.getRecord();
        List<Attachment>lstAttachment = [SELECT Id, Name FROM Attachment WHERE parent.Id = :this.accRecord.Id];
        imgWrapper = new List<imageWrapper>();
        
        for(Attachment attach:lstAttachment){
            imgWrapper.add(new imageWrapper(attach.Id,attach.Name));
        }
    }
    
    public String emailIDString {get;set;}
    
    public void sendMail(){
    	set<Id> setID = new Set<id>();
    	
        if(String.isEmpty(emailIDString)){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'email address is blank');
            ApexPages.addMessage(myMsg);
            return;
        }
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {emailIDString});
        mail.setSubject('Images of properties');
        mail.setPlainTextBody('PFA the images of the properties we discussed');
        
        List<Messaging.EmailFileAttachment> lstSendAttachment = new List<Messaging.EmailFileAttachment>();
        for(imageWrapper wrapper:imgWrapper){
            if(wrapper.isChecked){        
            	setID.add(wrapper.imageID);
            }
        }
        
        for(Attachment att:[SELECT Id, Body, Name FROM Attachment WHERE Id IN :setID]){

         	Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
            attachment.SetBody(att.body);
            attachment.setFileName(att.Name);
            lstSendAttachment.add(attachment);
        }
        
        mail.setFileAttachments(lstSendAttachment);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}