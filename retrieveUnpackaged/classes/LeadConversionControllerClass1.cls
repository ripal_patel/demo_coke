public class LeadConversionControllerClass1{

public List<Lead> lstlead = new List<Lead>();
public List<Contact> lstcon = new List<Contact>();
public List<Contact> lstupdatecontact = new List<Contact>();
public string email{get;set;}
public String lastname{get;set;}
public String company{get;set;}
public String country{get;set;}
public String city{get;set;}
public String phone{get;set;}
public Lead l{get;set;}
public String name{get;set;} 

public LeadConversionControllerClass1(ApexPages.StandardController controller)
{    
  l = new Lead();
}

public void convert(){

        PageReference pg = new PageReference('/apex/Convertlead');
        pg.setRedirect(true);
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Do You Want to Create New Lead'));
        
       
        }
        
/*   else{
   
         
        
       
  }*/
  
 public void yes(){ 
 
      lstlead = [SELECT Id,LastName,Company,Country,City,Phone,Email FROM Lead WHERE LastName=:l.lastname ];
   if(lstlead.size() <= 0) {
        Lead myLead = new Lead();
        myLead.LastName=l.lastname;
        myLead.Company=l.company;
        myLead.Country=l.country;
        myLead.City=l.city;
        myLead.Phone=l.phone;
        myLead.Email=l.email;
        insert myLead;
     }
   }
  
  public void no(){ 
  
      
      lstlead = [SELECT Id,LastName,Company,Country,City,Phone,Email FROM Lead WHERE LastName=:l.lastname ];
   if(lstlead.size() <= 0) {
        Lead myLead = new Lead();
        myLead.LastName=l.lastname;
        myLead.Company=l.company;
        myLead.Country=l.country;
        myLead.City=l.city;
        myLead.Phone=l.phone;
        myLead.Email=l.email;
        insert myLead;
     }
  
       lstcon = [select id, name, phone from Contact where LastName=:l.lastname limit 1];
       
         if(lstcon.size() > 0) {
          for(Contact objCont : lstCon)
          {
                  objCont.LastName= l.lastname;
                  objCont.Phone= l.phone;
                  objCont.Email=l.email;    
                  lstupdatecontact.add(objCont);  
          }    
                  
          if(lstupdatecontact.size()>0)
          {
               update lstupdatecontact;
          }
            PageReference pg1 = new PageReference('/apex/Convertlead');
            pg1.setRedirect(true);
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Contact updated Successfully.Thank you!'));
       }
        
        else {
        
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(lstlead[0].id);

        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);

        Database.LeadConvertResult lcr = Database.convertLead(lc);
        System.assert(lcr.isSuccess());  
        
        PageReference pg2 = new PageReference('/apex/Convertlead');
        pg2.setRedirect(true);
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Lead Converted Successfully.Thank you!')); 
        
        }
     }
  }
  /*public PageReference doCancel()
  {
    
  }*/