public class AccountSelectClassController123{
 
   public Contact con{get;set;}
    public Opportunity opportunity{get;set;}
    public Contact contact{get;set;}
    public String OppId{get;set;}
    
     public AccountSelectClassController123(ApexPages.StandardController controller)
     {    
        // ApexPages.currentPage().getParameters().get('OppId')
         this.opportunity= (opportunity)controller.getrecord();
         system.debug('*************Opportuniy' +opportunity );
          system.debug('*************Opportuniyiod' +OppId );
          con = new Contact();
     }
  
   public PageReference save() {
     insert con;
     ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Record Created Successfully.Thank you!'));
     return null;
   }
   
   
   /*public PageReference addRecord(){
   Record record=new Record (OpportunityContactRoleId=[SELECT Id FROM OpportunityContactRole WHERE SobjectType='Record'LIMIT='1'].Id)
   try{
   insert record;
   }
   catch(Exception ex){}
   return new PageReference ('/' + record.Id);
   }*/
   
  public List <utilityContact> opportunityContactRoleList {get; set;}
    
    //Returning a list of Contact Roles related to this Opp
    public PageReference getRelevantOpportunityContactRoles() { 
        system.debug('*inside');
         system.debug('*con action********' + con);
        if (opportunityContactRoleList == null){
            
           opportunityContactRoleList = new List <utilityContact> ();
          System.debug('*[SELECT Id, ContactId, Role, Contact.Name, Contact.Email FROM OpportunityContactRole WHERE OpportunityId =:con.Opportunity__c]*' + [SELECT Id, ContactId, Role, Contact.Name, Contact.Email FROM OpportunityContactRole WHERE OpportunityId =:con.Opportunity__c]);
        
           for (OpportunityContactRole eachOpportunityContactRole : [SELECT Id, ContactId, Role, Contact.Name, Contact.Email FROM OpportunityContactRole WHERE OpportunityId =:con.Opportunity__c]){
                    
            opportunityContactRoleList.add(new utilityContact(eachOpportunityContactRole));
               
            }
            
        }
        System.debug('**opportunityContactRoleList*' + opportunityContactRoleList);
        return null;
     }   
     
 
   public class utilityContact{
       public OpportunityContactRole acc {get; set;}
        public Boolean selected {get; set;}
 
        public utilityContact(OpportunityContactRole a) {
            acc = a;
            selected = false;
        }
    }
 
    public string DeleteSelectedRecs(){
      List<sObject> DeleteList = New List<sObject>();
      for(utilityContact wc:opportunityContactRoleList ){
          if(wc.selected == true)
             DeleteList.add(wc.acc); 
      }
      delete DeleteList;
      List<utilityContact > clone1= New List<utilityContact >();
       for(utilityContact wc:opportunityContactRoleList ){
          if(wc.selected == false)
             clone1.add(wc); 
      }
      opportunityContactRoleList.clear();
      opportunityContactRoleList  = clone1.clone();
      return null;     
    }
    
    public PageReference AddSelectedRecs()
    {  
        PageReference pageRef= new PageReference('https://ap1.salesforce.com/p/opp/ContactRoleEditUi/e?/con.Opportunity__c');
        pageRef.setredirect(true);       
        return pageRef;          

    }
}