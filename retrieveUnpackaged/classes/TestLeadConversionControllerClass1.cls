/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestLeadConversionControllerClass1 {

    static testMethod void TestLeadConversionControllerClass1() {
    	Lead l = [SELECT ID FROM Lead LIMIT 1];
    	
    	
    	test.startTest();
    	Test.setCurrentPage(Page.LeadConversion1);
    	// TO DO: implement unit test
        Lead objLead = new Lead();
        objLead.LastName = 'Test';
        objLead.Company = 'TestCompany';
        objLead.Phone = '22222';
        objLead.Email = 'ttt@gmail.com';
        objLead.City = 'testcity1';
        objlead.Country = 'testCountry1';
        insert objLead;
        
        Contact objContact = new Contact();
        objContact.LastName ='TestName';
        objContact.Phone = '12345';
        objContact.Email = 'test@gmail.com';
        objContact.MailingCity = 'testcity';
        objContact.MailingCountry = 'TestCountry';
   
        insert objContact;
        
         //ApexPages.currentPage().getParameters().put('l',objLead.id);
         List<lead> leadList = [SELECT Name FROM Lead LIMIT 20];
         ApexPages.StandardController stdLead = new ApexPages.StandardController(objLead);
         LeadConversionControllerClass1 objLeadConversionControllerClass1 = new  LeadConversionControllerClass1(stdLead);
         
         objLeadConversionControllerClass1.convert();
         objLeadConversionControllerClass1.yes();
         objLeadConversionControllerClass1.no();
         
       	test.stoptest();
    }
}