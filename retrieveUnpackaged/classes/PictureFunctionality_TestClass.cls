/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@istest
public with sharing class PictureFunctionality_TestClass {

 public static testmethod void DoPictureFunctionality()
 {
  Account Ac = new Account();
  Ac.Name = 'Test Gallery Account';
 
  insert Ac;
  
  Attachment Atch = new Attachment();
  Atch.Name = 'Img1';
  Atch.Body = Blob.valueOf('Img1');
  Atch.ParentId =Ac.Id;
  insert Atch;
  
  Attachment Atch1 = new Attachment();
  Atch1.Name = 'Img2';
  Atch1.Body = Blob.valueOf('Img2');
  Atch1.ParentId =Ac.Id;
  insert Atch1;
 
 
  Attachment Atch2 = new Attachment();
  Atch2.Name = 'Img3';
  Atch2.Body = Blob.valueOf('Img3');
  Atch2.ParentId =Ac.Id;
  insert Atch2;
 
  Pagereference Ref = page.img ;
  Test.setCurrentPage(Ref);
  ApexPages.StandardController sc = new ApexPages.standardController(Ac);
  
  oceanPropertiesExtension  imgwrp = new oceanPropertiesExtension(sc);
  imgwrp.imgWrapper.get(0).isChecked = true;
  imgwrp.emailIDString = 'ripal.patel@eternussolutions.com'; 
  imgwrp.sendMail();
  
  imgwrp.emailIDString = ''; 
  imgwrp.sendMail();
  
  list <Attachment> lstatch = new list<Attachment>();
  lstatch = [Select a.Name, a.Id From Attachment a Where a.ParentId = :Ac.Id];
  system.assertEquals(3, lstatch.size());
  }
}