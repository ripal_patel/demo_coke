public with sharing class CountryLang {

//Variables for used for controller
public list<Account> lstAccounts{get;set;}
public String country {get;set;}
public String language {get;set;}
public Boolean showAccounts{get;set;}

 
 public map<String,Country__c> countrymap;
 public map<String,Language__c> languagemap;   
 
 public CountryLang(){
   showAccounts = false;
   countrymap = Country__c.getAll();
   languagemap = Language__c.getAll();
  
      // country = countrymap.values();
  // language = languagemap.values();
 }
 
 // Create the select options for the two select lists on the page
 public List<Selectoption> getcountrylist(){
  List<Selectoption> op = new List<Selectoption>();
  op.add(new SelectOption('', '-- Select Country --'));        
  List<String> countryNames = new List<String>();
        countryNames.addAll(countrymap.keySet());
        countryNames.sort();
  
  for (String countryName : countryNames) {
            Country__c country = countrymap.get(countryName);
            op.add(new SelectOption(country.Name, country.Name));
        }
  return op;
 }
 
 public list<Selectoption> getlanguagelist(){
  List<SelectOption> options = new List<SelectOption>();
  Map<String, Language__c> states = new Map<String, Language__c>();
  Map<String, Language__c> allstates = Language__c.getAll();
  
  for(Language__c state : allstates.values()) {
            if (state.country__c == this.country) {
                states.put(state.name, state);
            }
        }
        
  //To display the fields in sorted order
  List<String> stateNames = new List<String>();
        stateNames.addAll(states.keySet());
        stateNames.sort();
  
  for (String stateName : stateNames) {
            Language__c state = states.get(stateName);
            options.add(new SelectOption(state.Name, state.Name));
        }
  if (options.size() > 0) {
            options.add(0, new SelectOption('', '-- Select Language --'));
        } else {
            options.add(new SelectOption('', '       '));
        }
        return options;
  
 }
 
 //On Click of search button
 public void populateAccounts(){
     
      System.debug('***country *' + country );
    lstAccounts = new list<Account>();
    if(country == ''){
        lstAccounts = new list<Account>();
    }
    if(country <> ''){
        //System.debug('***lstAccounts *' + lstAccounts );
        for(Account account : [SELECT Id, Name, Phone,Billingcity, BillingCountry, Language__c  
                               FROM Account 
                               WHERE BillingCountry  = : country ]){
        lstAccounts.add(account);   
    }  
    }                           
                              
     System.debug('***lstAccounts *' + lstAccounts );
    if(!lstAccounts.isEmpty())
        showAccounts = true;
 }
}