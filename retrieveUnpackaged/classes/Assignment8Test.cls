/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@istest
public with sharing class Assignment8Test {
 	static testmethod void DoAssignment8()
  	{
		  ApexAssignment assign = new ApexAssignment();
		  Account accNew = new Account(Name='Aabc');
		  accNew.Phone = '9405791062';
		  accNew.Email_Id__c = 'Sales@123.com';
		  insert accNew;
		  
		  Contact cntNew = new Contact();
		  cntNew.FirstName='abc';
		  cntNew.LastName = 'dakljd';
		  cntNew.AccountId = accNew.id;
		  insert cntNew;
		   
		  test.starttest();
		   
		  string Result = assign.GetContacts('Aabc');
		  Contact Con = new Contact();
		  Con = [SELECT a.Name from Contact a where a.Account.Name='Aabc']; 
		  system.assertEquals(Result, Con.Name);
		   
		  boolean ret = assign.evenOdd(3);
		  system.assertEquals(ret, false);
		  
		  ret = assign.evenOdd(4);
		  system.assertEquals(ret, true);
		  
		  List<Integer>fib = assign.fibonacci(5);
		  system.assertEquals(0, fib.get(0));
		  system.assertEquals(1, fib.get(1));
		  system.assertEquals(1, fib.get(2));
		  system.assertEquals(2, fib.get(3));
		  system.assertEquals(3, fib.get(4));
		  
		  
		  Account accUpdate = new Account(Name='AupdateAcc');
		  accUpdate.Phone = '9405791062';
		  accUpdate.Email_Id__c = 'Sales@123.com';
		  insert accUpdate;
		  
		  assign.updateAccount('AupdateAcc');
		  List<Account> accList = [SELECT Name FROM Account WHERE Site='updated'];
		  system.assertEquals(1, accList.size());
		  
		  List<String> getAccList = assign.getAcc();
		  system.assertEquals(2, getAccList.size());
		  
		  assign.createAccount('saket', '123456', '98900');
		  Account acc = [SELECT Name, Id FROM Account WHERE Name='saket'];
		  Contact cont = [SELECT LastName FROM Contact WHERE AccountId=:acc.Id];
		  
		  system.assertEquals(cont.LastName, 'New contact');
		  
		  List<Account> lstRetAcc = assign.accountSort();
		  List<Account> lstSort = [SELECT a.Name FROM Account a ORDER BY a.Name ASC];
		  
		  for(Integer i=0;i<lstRetAcc.size();i++){
		  	system.assertEquals(lstRetAcc.get(i), lstSort.get(i));
		  }
		  
		  List<String>getAccount = assign.getAccountName('abc');
		  List<Contact>lstCon = [SELECT c.Account.Name, c.Name FROM Contact c WHERE c.Name = 'abc'];
		  Integer iter = 0;
		  for(Contact c:lstCon){
		  	system.assertEquals(c.Account.Name, getAccount.get(iter));
		  	iter++;
		  }
		  
		  List<String>getContact = assign.getChildFromParent('Aabc');
		  List<Account>lstCont = [Select (Select Name, Email, Birthdate From Contacts), a.Name From Account a WHERE a.Name='Aabc'];
		  system.assertEquals(1, lstCont.size());
		  
		  
		  try{
		  	ApexAssignment objAssign = new ApexAssignment('empTest');
		  }
		  catch(System.QueryException ex){
		  	
		  }
		
		  test.stoptest();
  	}
}