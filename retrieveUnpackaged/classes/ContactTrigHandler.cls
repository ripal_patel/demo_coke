public with sharing class ContactTrigHandler {

	public void checkDuplicate(List<Contact> obj){
		List<Contact>lstContact = [SELECT cnt.Email, cnt.Phone FROM Contact cnt];
		for(Contact newCont:obj){
			for(Contact iter:lstContact){
				if(iter.Email == newCont.Email){
					newCont.addError('A contact with same email already exists');
					return;	
				}
				else if(iter.Phone == newCont.Phone ){
					newCont.addError('A contact with same phone number already exists');
					return;	
				}
			}
		}
	}
}