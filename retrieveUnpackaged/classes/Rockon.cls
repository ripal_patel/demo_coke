public with sharing class Rockon {
public list<Account> lstAccounts{get;set;}
 public String country {get;set;}
 public String language {get;set;}

 
 
 
 public map<String,Country__c> countrymap;
 public map<String,Language__c> languagemap;
 public Rockon(){
  
   countrymap = Country__c.getAll();
   languagemap = Language__c.getAll();
  
  // country = countrymap.values();
  // language = languagemap.values();
  
  
 }
 
 public List<Selectoption> getcountrylist(){
  List<Selectoption> op = new List<Selectoption>();
  op.add(new SelectOption('', '-- Select Country --'));        
  List<String> countryNames = new List<String>();
        countryNames.addAll(countrymap.keySet());
        countryNames.sort();
  
  for (String countryName : countryNames) {
            Country__c country = countrymap.get(countryName);
            op.add(new SelectOption(country.Name, country.Name));
        }
  return op;
 }
 
 public list<Selectoption> getlanguagelist(){
  List<SelectOption> options = new List<SelectOption>();
  Map<String, Language__c> states = new Map<String, Language__c>();
  Map<String, Language__c> allstates = Language__c.getAll();
  for(Language__c state : allstates.values()) {
            if (state.country__c == this.country) {
                states.put(state.name, state);
            }
        }
  
  List<String> stateNames = new List<String>();
        stateNames.addAll(states.keySet());
        stateNames.sort();
  
  for (String stateName : stateNames) {
            Language__c state = states.get(stateName);
            options.add(new SelectOption(state.Name, state.Name));
        }
  if (options.size() > 0) {
            options.add(0, new SelectOption('', '-- Select Language --'));
        } else {
            options.add(new SelectOption('', '       '));
        }
        return options;
  
 }
 public void populateAccounts(){
 
 
 
 
 }
}