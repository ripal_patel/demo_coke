/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestQue3 {
 static testMethod void myUnitTest() {
  
    
        
       Account a = new Account();
       a.Name = 'Parent11';
       a.Phone = '3216541';
       a.Email_Id__c = 'as1@as.com';
       insert a;
      
       Account a1 = new Account();
       a1.Name = 'Child 111';
       a1.Phone = '3216541';
       a1.Email_Id__c = 'as1@sa.com';
       insert a1;
        
        Account a2 = new Account();
        a2.Name = 'Child 211';
        a2.Phone = '3216541';
        a2.Email_Id__c = 'as1@sa.com';
        insert a2;      
 }
  
}