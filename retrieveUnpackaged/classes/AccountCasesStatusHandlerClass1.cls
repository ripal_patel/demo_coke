public with sharing class AccountCasesStatusHandlerClass1
{
	public void afterInsert (map<Id,Case> mapNewCase)
    {
		set<Id> setCaseAccountId = new set<Id>();//var define define case to get accnt id
    	
    	for(Case objCase : mapNewCase.Values())
    	{
    		System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>objCase.AccountId : '+objCase.AccountId);
    		setCaseAccountId.add(objCase.AccountId);
    	}
    	System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>setCaseAccountId : '+setCaseAccountId);
    	if(!setCaseAccountId.IsEmpty())//for null pointer exception
    	{
    	    rollUpMethod(setCaseAccountId);
    	}
    }
    public void rollUpMethod(set<Id> setCaseAccountId)
	{
		list<Account> lstAccountToUpdate = new list<Account>();
		
		map<Id, list<Case>> mapAccountID_lstCase = new map<Id, list<Case>>();
				
		for(Case objCase : [Select c.Status_Of_Cases_del__c, c.Id, c.AccountId From Case c 
    	                          where c.AccountId IN: setCaseAccountId])//to get acnt id, used for loop
    	{
    		if(!mapAccountID_lstCase.containsKey(objCase.AccountId))
    		{
    			mapAccountID_lstCase.put(objCase.AccountId, new list<Case> {objCase});
    		}
			else
			{
				mapAccountID_lstCase.get(objCase.AccountId).add(objCase);
			}
			System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>mapAccountID_lstCase : '+mapAccountID_lstCase);
    	}
    	
    	decimal sumOfOpenCases = 0;//counters dedfined
    	decimal sumOfClosedCases = 0;                         
    	
    	System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>setCaseAccountId : '+setCaseAccountId);
   
    	for(Account objAccount : [Select a.NoOfOpenCases__c, a.NoOfClosedCases__c, a.Id, a.AccountNumber, a.ParentId From Account a
    							 where a.Id IN : setCaseAccountId])
    	{
    		System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>objAccount : '+objAccount);
    		System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>ParentId : '+objAccount.ParentId);
    		if(mapAccountID_lstCase.get(objAccount.Id) != null)
    		{
	    		for(Case objCase : mapAccountID_lstCase.get(objAccount.Id))
	    		{
	    			if(objCase.Status_Of_Cases_del__c == 'Open Case')
	    			{
	    				sumOfOpenCases = sumOfOpenCases + 1;
	    				System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>sumOfOpenCases : '+sumOfOpenCases);
	    			}
	    			
	    			if(objCase.Status_Of_Cases_del__c == 'Closed Case')
	    			{
	    				sumOfClosedCases = sumOfClosedCases + 1;
	    				System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>sumOfClosedCases : '+sumOfClosedCases);
	    			}
	    			
	    		}
    		}
    		objAccount.NoOfOpenCases__c = sumOfOpenCases;
    		System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>objAccount.NoOfOpenCases__c : '+objAccount.NoOfOpenCases__c);
    		objAccount.NoOfClosedCases__c = sumOfClosedCases;
    		System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>objAccount.NoOfClosedCases__c : '+objAccount.NoOfClosedCases__c);
    		lstAccountToUpdate.add(objAccount);
    		System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>lstAccountToUpdate : '+lstAccountToUpdate);
    	}
    	if(!lstAccountToUpdate.IsEmpty())
    	{
    		update lstAccountToUpdate;//acnt is updated
    	}
	} 
	public void afterUpdate (map<Id,Case> mapNewCase , map<Id,Case> mapOldCase)
    {
		set<Id> setCaseAccountId = new set<Id>();
    	
    	for(Case objCase : mapNewCase.Values())
    	{
    		if(mapNewCase.get(objCase.id).Status_Of_Cases_del__c != mapOldCase.get(objCase.id).Status_Of_Cases_del__c)          
    		{
	    		System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>objCase.AccountId : '+objCase.AccountId);
	    		setCaseAccountId.add(objCase.AccountId);
    		}
    	}
    	System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>setCaseAccountId : '+setCaseAccountId);
    	if(!setCaseAccountId.IsEmpty())
    	{
    	    rollUpMethod(setCaseAccountId);
    	}
    }
    
    public void afterDelete(map<Id,Case> mapOldCase )
    {
    	set<Id> setCaseAccountId = new set<Id>();
    	
    	for(Case objCase : mapOldCase.Values())
    	{
    	   setCaseAccountId.add(objCase.AccountId);
    	}
    	if(!setCaseAccountId.IsEmpty())
    	{
    		rollDownMethod(setCaseAccountId,mapOldCase);
    	}
    }
    
    public void rollDownMethod(set<Id> setCaseAccountId,map<Id,Case> mapOldCase)
    {
    	
    	list<Account> lstAccountToUpdate = new list<Account>();
    	//list<Account>lstAcc=new list<Account>();
    	
    	//decimal NoOfClosedCases = 0;
    	
    	//value=lstAcc.NoOfClosedCases__c ;
    	
    	for(Account objAccount : [Select a.NoOfOpenCases__c, a.NoOfClosedCases__c, a.Id, a.AccountNumber, a.ParentId From Account a
    							 where a.Id IN : setCaseAccountId])
		 {
		 	decimal ValuesOPEN=0;
		 	decimal ValuesCLOSE=0;
		 	for(Case objCase : mapOldCase.Values())
		 	{
		 		if(objCase.Status_Of_Cases_del__c == 'Open Case')
		 		{
		 			System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>objAccount : '+objAccount);
		 	
				 	ValuesOPEN = objAccount.NoOfOpenCases__c;
				 	ValuesOPEN= ValuesOPEN-1;
				 	objAccount.NoOfOpenCases__c = ValuesOPEN;
				 	lstAccountToUpdate.add(objAccount);
		 		}
		 		if(objCase.Status_Of_Cases_del__c == 'Closed Case')
		 		{
		 			System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>objAccount : '+objAccount);
		 	
				 	ValuesCLOSE = objAccount.NoOfClosedCases__c;
				 	ValuesCLOSE= ValuesCLOSE-1;
				 	objAccount.NoOfClosedCases__c = ValuesCLOSE;
				 	lstAccountToUpdate.add(objAccount);
		 		}
		 	}
		 	
		 }
    							 
		 if(!lstAccountToUpdate.IsEmpty())
         {
             update lstAccountToUpdate;//acnt is updated
         }
    	                         
    	                         //System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>mapAccountID_lstCase : '+mapAccountID_lstCase);
    	                         
    	                         
    }
        /*decimal sumOfOpenCases = 0;//counters dedfined
    	decimal sumOfClosedCases = 0;                         
    	
    	System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>setCaseAccountId :' +setCaseAccountId);
   
    	for(Account objAccount : [Select a.NoOfOpenCases__c, a.NoOfClosedCases__c, a.Id, a.AccountNumber, a.ParentId From Account a
    							 where a.Id IN : setCaseAccountId])
    	{
    		System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>objAccount : '+objAccount);
    		System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>ParentId : '+objAccount.ParentId);
    		if(mapAccountID_lstCase.get(objAccount.Id) != null)
    		{
	    		for(Case objCase : mapAccountID_lstCase.get(objAccount.Id))
	    		{
	    			if(objCase.Status_Of_Cases_del__c == 'Open Case')
	    			{
	    				sumOfOpenCases = sumOfOpenCases - 1;
	    				System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>sumOfOpenCases : '+sumOfOpenCases);
	    			}
	    			if(objCase.Status_Of_Cases_del__c == 'Closed Case')
	    			{
	    				sumOfClosedCases = sumOfClosedCases - 1;
	    				System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>sumOfClosedCases : '+sumOfClosedCases);
	    			}
	    		}
    		}
    		objAccount.NoOfOpenCases__c = sumOfOpenCases;
    		System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>objAccount.NoOfOpenCases__c : '+objAccount.NoOfOpenCases__c);
    		objAccount.NoOfClosedCases__c = sumOfClosedCases;
    		System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>objAccount.NoOfClosedCases__c : '+objAccount.NoOfClosedCases__c);
    		lstAccountToUpdate.add(objAccount);
    		System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>lstAccountToUpdate : '+lstAccountToUpdate);
    	}
    	if(!lstAccountToUpdate.IsEmpty())
    	{
    		update lstAccountToUpdate;//acnt is updated
    	}*/
}