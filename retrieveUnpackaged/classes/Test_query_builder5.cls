/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_query_builder5 {
 
 public Test_query_builder5(){
  Pagereference pageref = Page.assgn_query;
  test.setCurrentPage(pageref);
  
 }
    static testMethod void myUnitTest_object() {
        // TO DO: implement unit test
        Query_builder_controller obj = new Query_builder_controller();
        List<Selectoption> l;        
        l = obj.getobjectlist();
        System.assertNotEquals(l.size(), 0);
        
    }
    static testMethod void myUnitTest_field(){
     Query_builder_controller obj = new Query_builder_controller();
     obj.objectnm = 'employee__c';
     List<Selectoption> l;
     l=obj.getfieldlist();
     System.assertNotEquals(l.size(), 0);
     
    }
    static testMethod void myUnitTest_limit(){
     Query_builder_controller obj = new Query_builder_controller();   
     List<Selectoption> l;  
     l=obj.getlimitlist();
     System.assertEquals(l.size(),4);     
    }
    static testMethod void myUnitTest_order(){
     Query_builder_controller obj = new Query_builder_controller();
  List<Selectoption> l;    
     l=obj.getorderlist();
     System.assertEquals(l.size(),2);
     system.assertEquals(l.get(0).getValue(), 'asc');  
    }
    static testMethod void myUnitTest_execute(){
     Query_builder_controller obj = new Query_builder_controller();
  List<Selectoption> l;    
     obj.setval.add(new Selectoption('Id','Id'));
     obj.setval.add(new Selectoption('Name','Name'));
     obj.setval.add(new Selectoption('LastName','LastName'));
     obj.objectnm = 'Lead';
     obj.orderby = 'asc';
     obj.limitval = '500';
     
     obj.execute();
     System.debug('****'+obj.query);   
     System.assertEquals(obj.query, 'select Id, Name, LastName from Lead order by Id asc limit 500');
     
       
    }
    static testMethod void myUnitTest_execquery(){
     Query_builder_controller obj = new Query_builder_controller();
     
  obj.query = 'select Id, Name, LastName from Lead order by Id asc limit 500';
          
    }
}