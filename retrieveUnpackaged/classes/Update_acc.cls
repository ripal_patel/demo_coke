public class Update_acc{
 public void updateAccount(String accName){
        Account acc = new Account();
        List<Account> accList = new List<Account>();
        accList = [SELECT a.Name FROM Account a WHERE a.Name = :accName];
        for(Account a:accList){
            a.Site = 'updated';
        }
        update accList;
    }
}