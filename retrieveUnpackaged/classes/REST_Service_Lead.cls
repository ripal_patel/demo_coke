@RestResource (urlMapping='/Lead/*')
global with sharing class REST_Service_Lead {
 
 global class wrapper{
 public boolean isSuccess;
public  String lead;
 public String status;
  
  public wrapper(){
   isSuccess = false;   
  }
  public wrapper(boolean s,String id,String st){
   this.isSuccess = s;
   this.lead = id;
   this.status = st;
  }
 }
 
 
 
 @HttpPost
 global static wrapper doPost(String fname, String lname, String email, String phone)
 {
  List<wrapper> wrap;
  wrap = new List<wrapper>();
  boolean success;
  String lead;
  String status;
  
  Lead l = new Lead();
  
  l.FirstName = fname;
  l.LastName = lname;
  l.Email = email;
  l.Phone = phone;
  l.Company = 'ASD';
  l.Status = 'Open - Not Contacted';
  database.saveResult sr = database.insert(l);
  if(sr.isSuccess() == true){
   success = true;
   lead = sr.getId();
   status = 'Success';
  }
  else
  {
   success = false;
   lead = '';
   status = 'DML error occured';
  }
  wrap.add(new wrapper(success,lead,status));
  //insert l;
  return wrap[0];
 }
 
 @HttpDelete
 global static wrapper doDelete()
 {
  RestRequest req = RestContext.request;
  RestResponse res = RestContext.response;
  //String parameters =  req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
  //List<String> parts =  req.requestURI.split('//');
  //Account account = [SELECT Id FROM Account WHERE Id = :accountId];
  //delete account;
  // = url .split('//'); 
  //List<String> p = parts[1].split('//'); 
  List<wrapper> wrap;
  wrap = new List<wrapper>();
  boolean success;
  String lead;
  String status;
  
  String fname = req.params.get('fName');
  String lname = req.params.get('lName');
  String email = req.params.get('email');
  String phone = req.params.get('phone');
  
  Lead l = [select Id,Name from lead where FirstName =:fname and LastName = :lname and Email = :email and Phone = :phone limit 1];
    
  
  database.Deleteresult dr = database.delete(l);
  if(dr.isSuccess() == true){
   success = true;
   lead = l.Name;
   status = 'Success';
  }
  else
  {
   success = false;
   lead = '';
   status = 'DML error occured for delete';
  }
  wrap.add(new wrapper(success,lead,status));
  return wrap[0];
 } 
 
 @HttpPut
 global static wrapper doPut(String fname,String lname,String email,String phone){
  
  RestRequest req = RestContext.request;
  RestResponse res = RestContext.response;
  List<wrapper> wrap;
  wrap = new List<wrapper>();
  boolean success;
  String lead;
  String status;
 /* 
  String fname = req.params.get('fName');
  String lname = req.params.get('lName');
  String email = req.params.get('email');
  String phone = req.params.get('phone');*/
  List<Lead> newLead = new List<Lead>();
  Lead l=[select Email,Phone from Lead where FirstName=:fname and LastName=:lname limit 1];
  l.Email =email;
  l.Phone = phone;
  /*List<Lead> llead = new List<Lead>();
  llead.add(l); 
  */
  Database.Saveresult sr = Database.update(l);
  if(sr.isSuccess()==true){
   success = true;
   lead = sr.getId();
   status = 'Success';
  }
  else
  {
   success = false;
   lead = '';
   status = 'DML error occured for update';
  }
  wrap.add(new wrapper(success,lead,status));
  return wrap[0];
  
 }
 

}