public with sharing class ContStud {

 public string lang {get;set;}
    public ContStud(ApexPages.StandardController controller) {

    }
    
    public List<Selectoption> getlanglist(){
        
        List<Selectoption> op = new List<Selectoption>();
        op.add(new Selectoption('English','English'));
        op.add(new Selectoption('French','French'));
        op.add(new Selectoption('Spanish','Spanish'));
        return op;
    }
    
    public void changelang()
    {
     User use = [select ID, LanguageLocaleKey from User where ID = :UserInfo.getUserId()];
     System.debug(lang);
     if(lang == 'French')
     {
     use.LanguageLocaleKey = 'fr';
     update use;
     }
     else if(lang == 'Spanish')
     {
     use.LanguageLocaleKey = 'es';
     update use;
     }
     else
     {
     use.LanguageLocaleKey = 'en_US';
     update use;
     }
     
    }
    
}