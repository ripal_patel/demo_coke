public with sharing class ScheduledOpportunityAmountAggregator 
{
  public String selectedSeconds {get;set;}
  public String selectedHours {get;set;}
  public String selectedMinutes {get;set;}
  public String selectedDayMonth {get;set;}
  public String selectedDayWeek {get;set;}
  public String selectedMonth {get;set;}
  public String selectedYear {get;set;}
  integer i;
  String str,str1;
  public List<SelectOption> getSecondMinuteValue()
    {   
        List<SelectOption> secondminutes = new List<SelectOption>();
        for(i=0;i<60;i++)
        {
            secondminutes.add(new SelectOption(String.ValueOf(i),String.ValueOf(i)));
        }
       return secondminutes;
     }
     
      public List<SelectOption> getHourValue()
      {   
        List<SelectOption> hours = new List<SelectOption>();
        for(i=0;i<24;i++)
        {

            hours.add(new SelectOption(String.ValueOf(i),String.ValueOf(i)));
        }
       return hours;
      }
      public List<SelectOption> getDayMonthValue()
      {   
        List<SelectOption> dayOfmonth = new List<SelectOption>();
        dayOfmonth.add(new SelectOption('-------------None-------------','-------------None-------------'));
        for(i=1;i<=31;i++)
        {

            dayOfmonth.add(new SelectOption(String.ValueOf(i),String.ValueOf(i)));
        }
       return dayOfmonth;
      }
      public List<SelectOption> getMonthValue()
      {   
        List<SelectOption> month = new List<SelectOption>();
        month.add(new SelectOption('----------None-----------','-------------None-------------'));
        month.add(new SelectOption('JAN','JAN'));
        month.add(new SelectOption('FEB','FEB'));
        month.add(new SelectOption('MAR','MAR'));
        month.add(new SelectOption('APR','APR'));
        month.add(new SelectOption('MAY','MAY'));
        month.add(new SelectOption('JUN','JUN'));
        month.add(new SelectOption('JUL','JUL'));
        month.add(new SelectOption('AUG','AUG'));
        month.add(new SelectOption('SEP','SEP'));
        month.add(new SelectOption('OCT','OCT'));
        month.add(new SelectOption('NOV','NOV'));
        month.add(new SelectOption('DEC','DEC'));
       return month;
      }
       public List<SelectOption> getDayWeekValue()
      {   
        List<SelectOption> dayweek = new List<SelectOption>();
        dayweek.add(new SelectOption('----------None-----------','----------None-----------'));
        dayweek.add(new SelectOption('SUN','SUN'));
        dayweek.add(new SelectOption('MON','MON'));
        dayweek.add(new SelectOption('TEU','TEU'));
        dayweek.add(new SelectOption('WED','WED'));
        dayweek.add(new SelectOption('THU','THU'));
        dayweek.add(new SelectOption('FRI','FRI'));
        dayweek.add(new SelectOption('SAT','SAT'));
       return dayweek;
      }
      
      public List<SelectOption> getYearValue()
      {   
        List<SelectOption> year = new List<SelectOption>();
        year.add(new SelectOption('----------None-----------','----------None-----------'));
        for(i=1970;i<=2014;i++)
        {
         year.add(new SelectOption(String.ValueOf(i),String.ValueOf(i)));
        }
       return year;
      }
      
      public void preview()
      {
        if(selectedDayMonth=='-------------None-------------')
        {
         selectedDayMonth='?';
        }
        if(selectedDayWeek=='----------None-----------')
        {
         selectedDayWeek='?';
        }
         if(selectedMonth=='----------None-----------')
        {
         selectedMonth='*';
        }
        if(selectedYear=='----------None-----------')
        {
         selectedYear='*';
        }
        str=selectedSeconds+' '+selectedMinutes+' '+ selectedHours+' '+selectedDayMonth+' '+selectedMonth+' '+selectedDayWeek+' '+selectedYear;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info,'Constructed Cron Expression:'+str));
        return;
      }
      public void ScheduleOpportunity()
      {
       OpportunityAmountAggregatorSchedular m = new OpportunityAmountAggregatorSchedular();
  String jobID = system.schedule('Schedular for opportunity1', str, m);
  return;
      }
     
}