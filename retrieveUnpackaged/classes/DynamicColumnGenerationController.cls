public with sharing class DynamicColumnGenerationController {
 
 public list<Account> lstAccounts{get;set;}
 public String tabName{get;set;}
 public Integer countOfRecords{get;set;}
 public Boolean showTracking{get;set;}
 
 public DynamicColumnGenerationController(){
  //showTracking= false;
  
 }//End Constructor



 public void showAccount(){
  
  if(countOfRecords > 0 ){
   
   system.debug('******Inside*****' + showTracking);
   
  }
  //showTracking = false;
  system.debug('******countOfRecords*****' + countOfRecords);
  String query = 'Select Id, Name From Account limit ' + countOfRecords;
  lstAccounts = Database.query(query);
  //lstAccounts = [Select Id, Name From Account limit 1];
  system.debug('******lstAccounts*****' + lstAccounts);
  
 }


}