global class OppAmountAgg implements Database.Batchable<SObject>,Database.Stateful

{
   String email;
   global double totalSize = 0;
   Time StartTime;
   global final String Query;
   global string stag='Closed Won';
   global integer sum_of_oppor_amount;
   String q = 'SELECT StageName, IsAggregated__c, Amount FROM Opportunity  WHERE StageName  =\'Closed Won\' AND CreatedDate=Today';
   User u=[Select Email from User where ProfileId='00e90000001PMUZ'];
   global OppAmountAgg()
   {
             Query=q; 
             sum_of_oppor_amount=0;
            StartTime=Datetime.now().time();
             
   }
   
  global Database.QueryLocator start(Database.BatchableContext BC)
   {
       return Database.getQueryLocator(Query);
   }

   global void execute(Database.BatchableContext BC,  List<sObject> scope)
   {
     List<Opportunity> opportunities = new List<Opportunity>();
        for(sObject sobjectIter : scope){
            Opportunity opportunity = (Opportunity)sobjectIter;
            opportunity.IsAggregated__c = true;
            if(opportunity.Amount != null){
                 sum_of_oppor_amount = (Integer)(sum_of_oppor_amount+opportunity.Amount);
                opportunities.add(opportunity);
            }
        }
        totalSize = opportunities.size();
        update opportunities; 
   }
   
  

   global void finish(Database.BatchableContext BC)
   {
        System.debug('The sum is:'+sum_of_oppor_amount);
    
         Time EndTime;
        
         AsyncApexJob apexJob = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id = :BC.getJobId()];
         if(apexJob.status=='Completed')
            EndTime = Datetime.now().time();
            
         Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
         mail.setToAddresses(new String[] {email});
     
         mail.setSenderDisplayName('Batch Processing');
         mail.setSubject('Daily Opportunity Amount Aggregator Job Complete');
      
         mail.setPlainTextBody('===StartTime==='+StartTime+'\n'+'===EndTime==='+EndTime+'\n'+' Number of Opportunities Closed Won Today: '+totalSize+'\n'+' Total Opportunity Amount for today: '+ sum_of_oppor_amount );
         Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
 }
   

}