public with sharing class ContPicklist {
  
 public String country {get;set;}
 public String city {get;set;}
 
 public map<String,Country__c> countrymap;
 public map<String,City__c> citymap;
 public ContPicklist(){
  
   countrymap = Country__c.getAll();
   citymap = City__c.getAll();
  
  // country = countrymap.values();
  // city = citymap.values();
  
  
 }
 
 public List<Selectoption> getcountrylist(){
  List<Selectoption> op = new List<Selectoption>();
  op.add(new SelectOption('', '-- Select Country --'));        
  List<String> countryNames = new List<String>();
        countryNames.addAll(countrymap.keySet());
        countryNames.sort();
  
  for (String countryName : countryNames) {
            Country__c country = countrymap.get(countryName);
            op.add(new SelectOption(country.Name, country.Name));
        }
  return op;
 }
 
 public list<Selectoption> getcitylist(){
  List<SelectOption> options = new List<SelectOption>();
  Map<String, City__c> states = new Map<String, City__c>();
  Map<String, City__c> allstates = City__c.getAll();
  for(City__c state : allstates.values()) {
            if (state.country__c == this.country) {
                states.put(state.name, state);
            }
        }
  
  List<String> stateNames = new List<String>();
        stateNames.addAll(states.keySet());
        stateNames.sort();
  
  for (String stateName : stateNames) {
            City__c state = states.get(stateName);
            options.add(new SelectOption(state.Name, state.Name));
        }
  if (options.size() > 0) {
            options.add(0, new SelectOption('', '-- Select City --'));
        } else {
            options.add(new SelectOption('', '       '));
        }
        return options;
  
 }
}