/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public with sharing class TestCloneAccount {

	public static testmethod void testClone(){
		
		Account acc = new Account();
		acc.Name = 'new account';
		acc.Phone = '123456';
		acc.Email_Id__c = '123@123.com';
		insert acc;
		
		Set<String> lstFields = new Set<String>{'name','phone','Email_Id__c'};
		
		Pagereference pgref = Page.testdemo;
		Test.setCurrentPage(pgref);
		ApexPages.StandardController sc = new ApexPages.standardController(acc);
		
		Apexpages.currentpage().getParameters().put('Id',acc.Id);
		
		Map<String,Schema.SObjectField> myObjectFields = Account.SObjectType.getDescribe().fields.getMap();
    	Set<String> myObjectFieldAPINames = myObjectFields.keyset();
		
		cloneAccount obj = new cloneAccount(sc);
		
		Integer fCount = 0;
		for(String fName:myObjectFieldAPINames){
			if(lstFields.contains(obj.wrapperLst.get(fCount).str)){
				//system.assert(false, obj.wrapperLst.get(fCount).str);
				obj.wrapperLst.get(fCount).flag=true;
			}
			fCount++;
		}
		
		obj.saveAccount();
		
		List<Account> lstAccount = [SELECT Id FROM Account WHERE Phone='123456'];
		system.assertEquals(lstAccount.size(), 2);
	}

}