public class DynamicBinding
{
    public Account account{get;set;}
    
    // constructor
    public DynamicBinding(){
        account = new Account();
    }
    
    // get sobject fields of account object
    public static List<String> getSobjectFields() 
       {
            List<String> fields = new List<String>();
            
            Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe();
            Schema.SObjectType sobjType = gd.get('account');
            Schema.DescribeSObjectResult r = sobjType.getDescribe();
            Map<String,Schema.SObjectField> M = r.fields.getMap();
            
            for(String Name : M.keyset())
                {
                    Schema.SObjectField field = M.get(Name);                                                    
                    Schema.DescribeFieldResult fieldDesc = field.getDescribe();
                    fields.add(Name.toLowerCase());
                }
            return fields;
       }   
}