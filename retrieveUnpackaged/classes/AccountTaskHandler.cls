public with sharing class AccountTaskHandler {
	public void createTask(List<Account> obj){
		List<Task>lstNewTask = new List<Task>();
		for(Account iter:obj){
			Task newTask = new Task(WhatId = iter.Id);
			newTask.Subject = 'Meeting with ' + iter.Name;
			lstNewTask.add(newTask);
		}
		insert lstNewTask;
	}
}