public with sharing class StudentController {

	public String selectedLang {get;set;}

	public StudentController(ApexPages.StandardController controller){
		
	}
	
	public void changeLanguage(){
		User nUser = [SELECT Id, LanguageLocaleKey FROM User WHERE Id=:UserInfo.getUserId()];
		system.debug(nUser);
		if(selectedLang == 'French'){			
			nUser.LanguageLocaleKey = 'fr';
		}	
		else if(selectedLang == 'Spanish'){
			nUser.LanguageLocaleKey = 'es';
		}
		else{
			nUser.LanguageLocaleKey = 'en_US';
		}
		update nUser;
	}
}