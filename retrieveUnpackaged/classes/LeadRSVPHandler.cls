public with sharing class LeadRSVPHandler {
	public void updateLeadRSVP(List<CampaignMember>cmpMembers){
		for(CampaignMember cmp:cmpMembers){
			if(String.isBlank(cmp.RSVP__c)){              
                Lead parentLead = [SELECT l.RSVP__c FROM Lead l WHERE l.Id = :cmp.LeadId];
				if(!String.isBlank(parentLead.RSVP__c)){
					cmp.RSVP__c = parentLead.RSVP__c;
				}
			}
		}
	}
}