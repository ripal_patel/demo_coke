public class AccountSelectClassController{
 

    Contact con = new Contact();
    
     public AccountSelectClassController(ApexPages.StandardController controller)
     {
        
     }
    
    Public Contact getCon()
    {
      return Con;
   }
   
   public PageReference save() {

     insert con;
     ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Record Created Successfully.Thank you!'));
      return null;
   }
   
  
  public List <utilityContact> opportunityContactRoleList {get; set;}
    
    //Returning a list of Contact Roles related to this Opp
    public List <utilityContact> getRelevantOpportunityContactRoles() { 
        System.debug('*******eachOpportunityContactRole*****');
        if (opportunityContactRoleList == null){
            
           opportunityContactRoleList = new List <utilityContact> ();
        
           for (OpportunityContactRole eachOpportunityContactRole : [SELECT Id, ContactId, Role, Contact.Name, Contact.Email FROM OpportunityContactRole WHERE OpportunityId = :ApexPages.currentPage().getParameters().get('oppId')]){
              System.debug('*******eachOpportunityContactRole'+eachOpportunityContactRole);
            opportunityContactRoleList.add(new utilityContact(eachOpportunityContactRole));
                
            }
            
        }
        System.debug('**opportunityContactRoleList*' + opportunityContactRoleList);
        return opportunityContactRoleList;
     }   
     
 
   public class utilityContact{
       public OpportunityContactRole acc {get; set;}
        public Boolean selected {get; set;}
 
        public utilityContact(OpportunityContactRole a) {
            acc = a;
            selected = false;
        }
    }
 
   
   
}