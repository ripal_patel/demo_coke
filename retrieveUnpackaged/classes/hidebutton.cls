public class  hidebutton {
 public String prefix {get;set;}
 public List<Dynamic_Button__c> lst {get;set;}
 public hidebutton(){
     lst = new List<Dynamic_Button__c>();
 }
 public void query(){
   prefix = Apexpages.currentPage().getParameters().get('prefix');
   lst = [Select Button_Name__c, Field_Id__c, Field_Value__c from Dynamic_Button__c where Object_Prefix__c =:prefix ];
     system.debug('\n\n prefix :- '+prefix );
     system.debug('\n\n lst :- '+lst);
 }
}