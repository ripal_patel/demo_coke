/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class testduptrig {

    public static TestMethod void testdup()
 {
  Contact a1 = new Contact();
  a1.Lastname = 'testnm';  
  a1.Phone = '94856';
  a1.Email='juju@tr.com';
  insert a1;
  List<Contact> al = new List<Contact>();
  for(Integer i = 0;i<195;i++)
  {
  Contact a = new Contact();
  a.Lastname = 'testnm'+i;  
  a.Phone = '94856';
  a.Email = 'xyz@gmail.com';
  al.add(a);
  }
  insert al;
 }
 
}