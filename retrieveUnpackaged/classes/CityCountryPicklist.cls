public with sharing class CityCountryPicklist {

    public String selectedCountry {get;set;}
    public String selectedCity {get;set;}
    
    public CityCountryPicklist(ApexPages.StandardController controller){
        
    }
    
    public List<SelectOption> getCountryList(){
        List<SelectOption> lstCountry = new List<SelectOption>();
        for(Country__c cnt:Country__c.getAll().values()){
            lstCountry.add(new SelectOption(cnt.Name, cnt.Name));
        }
        return lstCountry;
    }
    
    public List<SelectOption> getCityName(){
        List<SelectOption>cityList = new List<SelectOption>();
        
        for(City__c city:City__c.getAll().values()){
            if(city.Country__c == this.selectedCountry){
                cityList.add(New SelectOption(city.Name,city.Name));
            }
        }
        return cityList;
    }
}