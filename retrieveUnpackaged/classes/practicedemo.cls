public with sharing class practicedemo
{
	public void demoinsert(list <Extended_Bom_Component__c> pLstExtBomComp)
	{
		set <Id> setBomComponentId = new set<Id>();
		for(Extended_Bom_Component__c objbomcomp:pLstExtBomComp){
			setBomComponentId.add(objbomcomp.Bom_Component_Parent__c);
		}
		system.debug('~~~~~~~~~parent id'+setBomComponentId);
	}

}