global with sharing class TestPageController{
  public Decimal oppAmount {get; set;}
  public list<Opportunity> lstOpp {get; set;}
public TestPageController(){
    oppAmount = 0.0; 
    lstOpp = new list<Opportunity>();
    lstOpp  = [Select id, Amount ,Name from Opportunity];
    system.debug('###'+lstOpp );
    for(Opportunity opp : lstOpp  ){
    if(opp.Amount != null)
        oppAmount = oppAmount +opp.Amount ;
    }
    system.debug('###'+oppAmount );
}
}