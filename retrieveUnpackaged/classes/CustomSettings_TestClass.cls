/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@istest
public with sharing class CustomSettings_TestClass {
	
	public static testmethod void DoCustomSettings()
	{
		Country__c ctr =  new Country__c(Name = 'India');
		insert ctr;
		insert new Country__c(Name = 'US');
		insert new Country__c(Name = 'UK');
		
		
		insert new City__c(Name = 'Pune',Country__C='India');
		insert new City__c(Name = 'Mumbai',Country__C='India');
		insert new City__c(Name = 'Kolkata',Country__C='India');
		insert new City__c(Name = 'WC',Country__C='US');
		insert new City__c(Name = 'Colarido',Country__C='US');
		insert new City__c(Name = 'Lords',Country__C='UK');
		
		
		Pagereference Ref = page.Country_City;
		test.setCurrentPage(Ref);
		Apexpages.Standardcontroller ac = new Apexpages.Standardcontroller(ctr);
		
		
		CityCountryPicklist C = new CityCountryPicklist(ac);
		C.selectedCountry ='India';
		list<system.Selectoption> str = C.getCityName();
		system.assertEquals(str.size(), 3);
		
		C.getCountryList();
	
		
		C.selectedCountry ='US';
		list<system.Selectoption> str1 = C.getCityName();
		system.assertEquals(str1.size(), 2);
		
		C.selectedCountry ='UK';
		list<system.Selectoption> str2 = C.getCityName();
		system.assertEquals(str2.size(), 1);
	}
}