global class OpportunityAmountAggregatorSchedular implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        OppAmountAgg op=new OppAmountAgg();
        Database.executeBatch(op);
    }
}