public with sharing class CheckDuplicateContacts {
 public void CheckContacts(Contact obj)
 {
  list<Contact> con = new list<Contact>();
  con=[Select c.email,c.phone from Contact c];
  for(Contact contact :con)
  {
   if(contact.email == obj.Email)
   {
    obj.addError('Email Cant be duplicate');
   }
   else if(contact.phone == obj.Phone)
   {
    obj.addError('Phone cant be duplicate');
   }
  }
 }

}