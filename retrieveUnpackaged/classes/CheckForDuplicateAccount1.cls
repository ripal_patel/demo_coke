public with sharing class CheckForDuplicateAccount1 {
 public void CheckDuplicateAccount(list<Account> Acc)
 {
  
  for(Account ack:Acc)
  {
    list<Account> A = [Select a.Name,a.Phone, a.Email_Id__c From Account a WHERE a.Name = :ack.Name];   
    for(Account oldAcc : A)
    {
     if((oldAcc.Name==ack.Name &&(oldAcc.Phone==ack.Phone || oldAcc.Email_Id__c == ack.Email_Id__c))  ||  (oldAcc.Phone==ack.Phone &&(oldAcc.Name==ack.Name || oldAcc.Email_Id__c == ack.Email_Id__c))   ||   (oldAcc.Email_Id__c==ack.Email_Id__c &&(oldAcc.Phone==ack.Phone || oldAcc.Name == ack.Name)))
     {
      ack.ParentId = oldAcc.Id;  
     }
    } 
  }
 }
}