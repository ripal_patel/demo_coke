public with sharing class Query_builder_controller {
 public String objectnm {get;set;}
 public String fieldnm {get;set;}
 public String limitval{get;set;} 
 public List<Selectoption> setval {get;set;}
 public String orderby {get;set;}
 public String query ;
 public Query_builder_controller(){
  
  setval = new List<Selectoption>();
  
 }
 public List<Selectoption> getobjectlist(){
  List<Selectoption> op = new List<Selectoption>();
  op.add(new SelectOption('', '-- Select Object --'));        
  
  Map<String, Schema.SObjectType> objects = Schema.getGlobalDescribe();
    Schema.DescribeSObjectResult objInfo = null;
    for (Schema.SObjectType obj : objects.values()) {
      objInfo = obj.getDescribe();
        op.add(new Selectoption(objInfo.getName(),objInfo.getName())) ;
    }
  
  return op;
 }
 
 public List<Selectoption> getfieldlist(){
  List<Selectoption> op = new List<Selectoption>();
  op.add(new SelectOption('', '-- Select Field --'));        
  if(objectnm!=null){
  SObjectType accountType = Schema.getGlobalDescribe().get(objectnm);
  
  Map<String, Schema.SObjectField> fields = accountType.getDescribe().fields.getMap();
    Schema.DescribeFieldResult fieldInfo = null;
    for (Schema.SObjectField field : fields.values()) {
       fieldInfo = field.getDescribe();
        op.add(new Selectoption(fieldInfo.getName(),fieldInfo.getName())) ;
    }
  }
  return op;
 }
 
 public List<Selectoption> getlimitlist(){
  List<Selectoption> op = new List<Selectoption>();
  op.add(new SelectOption('500', '500'));
  op.add(new SelectOption('1000', '1000'));  
  op.add(new SelectOption('5000', '5000'));  
  op.add(new SelectOption('50000', '50000'));    
  
  return op;
 }
 public List<Selectoption> getorderlist(){
  List<Selectoption> op = new List<Selectoption>();
  op.add(new SelectOption('asc', 'Acsending'));
  op.add(new SelectOption('desc', 'Decending'));  
     
  
  return op;
 }
 public void execute(){
  String fieldsnm ;
  String temp;
  
  if(setval.size()!=0)
  {
   
  for(integer i = 0;i<setval.size();i++){
   
   
   if(i==setval.size()-1){
    fieldsnm +=setval.get(i).getValue(); 
   }
   else if(i==0){
    fieldsnm =setval.get(i).getValue(); 
    fieldsnm +=', ';
   }
   else{
    fieldsnm +=setval.get(i).getValue(); 
    fieldsnm +=', ';
   }
  }
  
  System.debug('********'+fieldsnm);
  }
  else
  {
   System.debug('**** null list');
  }
  
  System.debug('*****'+fieldsnm);
  
  query = 'select '+fieldsnm +' from '+ objectnm +' order by '+setval.get(0).getValue()+' ' + orderby + ' limit '+limitval  ;
  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,query));
  //executequeryreturn();
  
 }

 public List<sObject> getexecutequeryreturn(){
  List<sObject> L;
  if(query!=null)
  L = Database.query(query);
  return l;
  
 } 

}