public with sharing class AccountCasesStatusHandlerClass {
	
	public void afterInsert(list<Case> pLstCase){
    List<Case> lstCase = [select id, AccountId from Case where id in: trigger.newmap.keyset()];
        set<Id> sAccId = new set<Id>();
    
    for (Case cs: lstCase){
        if(cs.AccountId != null){
        	System.debug('*****lstCase***'+lstCase);
            sAccId.add(cs.AccountId);
        }
    }
    if(sAccId != null && sAccId.size() > 0){
        List<Account> lstAccount = [select id,NoOfOpenCases__c, (select id from Cases where status = 'Open') from Account where id in: sAccId];
        System.debug('*****lstAccount***'+lstAccount);
        if(lstAccount.size() > 0){
            for(Account acc: lstAccount){
                acc.NoOfOpenCases__c = acc.Cases.size();
            }
            
            update lstAccount;
        }
	}
}
	
	
    }