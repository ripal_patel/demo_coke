public with sharing class AccountTriggerHandler {
	public void AccountCheck(List<Account>newAcc){		
		List<Account> fetchAcc = [Select a.Name, a.Email_Id__c, a.Phone from Account a];
		if(fetchAcc.size()>=1){
			for(Account acc:newAcc){
				for(Account oldAcc:fetchAcc){
					if((acc.Name == oldAcc.Name && (acc.Email_Id__c == oldAcc.Email_Id__c || acc.Phone == oldAcc.Phone)) || (acc.Email_Id__c == oldAcc.Email_Id__c && (acc.Name == oldAcc.Name || acc.Phone == oldAcc.Phone)) || (acc.Phone == oldAcc.Phone && (acc.Name == oldAcc.Name || acc.Email_Id__c == oldAcc.Email_Id__c))){
						acc.ParentId = oldAcc.Id;
						break;
					}
				}
			}
		}
	}
}