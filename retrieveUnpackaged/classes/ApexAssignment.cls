public class ApexAssignment{
    
    public ApexAssignment(){
    	
    }
    
    public ApexAssignment(String str){
      	Employee__c e=new Employee__c();
      	e=[Select e.Is_Active__c, e.Full_Name__c, e.First_Name__c, e.Date_of_Birth__c, e.City__c, e.Age__c From Employee__c e where e.First_Name__c=:str Limit 1];
       	system.debug(e);
    }
    
    public String getContacts(String accName){
    	Contact c = new Contact();
        c = [SELECT c.Name FROM Contact c WHERE c.Account.Name= :accName];
       	system.debug(c.Name);
       	return C.Name;
	}
	
    public boolean evenOdd(Integer num){
		if(math.mod(num,2)==0){
			system.debug('Number is even');
			return true;
		}
		else{
			system.debug('Number is odd');
			return false;
		}
	}
    
    public void updateAccount(String accName){
    	Account acc = new Account();
        List<Account> accList = new List<Account>();
        accList = [SELECT a.Name FROM Account a WHERE a.Name = :accName];
        for(Account a:accList){
	    	a.Site = 'updated';
        }
        update accList;
    }
    
    public List<String> getAcc(){
   		List<String> strRet = new List<String>();
    	List<Account>accList = new List<Account>();
       	accList=[SELECT a.Name FROM Account a WHERE a.Name LIKE 'A%'];
       	for(Account acc:accList){
	        system.debug(acc.Name);
	        strRet.add(acc.Name);
       	}
    	return strRet;
    }
    
    public List<Integer> fibonacci(Integer limiter){
        Integer a=0,b=1,c;
        system.debug(string.valueOf(a));
        system.debug(string.valueOf(b));
        
        List<Integer> retInt = new List<Integer>();
        retInt.add(0);
        retInt.add(1);
        
        for(Integer temp=0;temp<limiter;temp++){
            c=a+b;
            system.debug(string.valueOf(c));
            retInt.add(c);
            a=b;
            b=c;
        }
        
        return retInt;
    }
    
    public void createAccount(String accName, String accNumber, String accPhone){          
      	Account newAccount = new Account(Name=accName, AccountNumber=accNumber, Phone=accPhone);
      	insert newAccount;
		Contact newContact = new Contact(LastName='New contact',Email='123@123.com',Phone='420',AccountID=newAccount.Id);
       	insert newContact;
   }
    
    public List<Account> accountSort(){
    	List<Account>accList = new List<Account>();
	    accList=[SELECT a.Name FROM Account a ORDER BY a.Name ASC];
	    for(Account acc:accList){
	    	system.debug(acc.Name);
	    }
	    
	    return accList;
    }
    
     public List<String> getAccountName(String contactName){
     	List<String>retList = new List<String>();
     	List<Contact>conList = new List<Contact>();
	    conList=[SELECT c.Account.Name, c.Name FROM Contact c WHERE c.Name = :contactName];
	    for(Contact con:conList){
	    	retList.add(con.Account.Name);
	    	system.debug(con.Account.Name);
	    }
	    
	    return retList;
    }
    
    public List<String> getChildFromParent(String accName){
    	List<String>retList = new List<String>();
      	List<Account>lstAcc = new List<Account>();
	  	lstAcc = [Select (Select Name, Email, Birthdate From Contacts), a.Name From Account a WHERE a.Name= :accName];
	  	for(Account iter:lstAcc){
	  		for(Integer counter=0;counter<iter.Contacts.size();counter++){
	  			retList.add(iter.Contacts[counter].Name);
	   			system.debug('Account: ' + iter.Name + 'Contact: ' + iter.Contacts[counter].Name);
	   		}
	   	}
	   	return retList;
    }
}