public with sharing class ExtendedBomComponentHandlerClass
{
	public void insertBefore(list<Extended_Bom_Component__c> pLstExtBomComp)	
	{
		map<Id,decimal>mapBomComponentwithquantity=new map<Id,decimal>();
		map<Id,Bom_component__c>mapcomponent=new map<Id,Bom_component__c>();
		set <Id> setBomComponentId = new set<Id>();
		
		list<Bom_component__c>pLstExtBomComp1=new list<Bom_component__c>(); 
		for(Extended_Bom_Component__c objbomcomp:pLstExtBomComp){
			setBomComponentId.add(objbomcomp.Bom_Component_Parent__c);
		}
		system.debug('***setBomComponentId***'+setBomComponentId);
		
		map <Id,Integer> BomComponentToUpdate = new map<Id,Integer>();
		list <Bom_component__c> lstBomComp = [Select Id, b.Unit_Price__c, b.Quantity__c, (Select Quantity__c, Unit_price__c 
		                                      From Extended_Bom_Components__r) From Bom_component__c b where id IN:setBomComponentId];
		       system.debug('***lstBomComp***'+lstBomComp);                               
		
		decimal quantity=0;
		
		for(Bom_component__c component : lstBomComp){
			quantity=0;
			for(Extended_Bom_Component__c extended : component.Extended_Bom_Components__r){
				if(quantity==0)
				{
					quantity=extended.Quantity__c;
				}
				else if(quantity>extended.Quantity__c)
					quantity=extended.Quantity__c;
				/*
				if(extended.Quantity__c > 100)
				{
				   extended.Quantity__c.addError('Error:Value should not exists beyond 100!');
				}*/
			}
			mapBomComponentwithquantity.put(component.Id,quantity);
		}
		system.debug('******mapBomComponentwithquantity***'+mapBomComponentwithquantity);
		
		list<Extended_Bom_Component__c>pLstExtBomComp3=new list<Extended_Bom_Component__c>(); 
		for(Extended_Bom_Component__c extended:pLstExtBomComp3){
			
			Integer Componentquantity = BomComponentToUpdate.get(extended.Bom_Component_Parent__c);
			if(Componentquantity > extended.Quantity__c){
				mapcomponent.put(extended.Bom_Component_Parent__c, new Bom_component__c( Id=extended.Bom_Component_Parent__c,
				                                       Quantity__c=extended.Quantity__c,
				                                       Unit_Price__c=extended.Unit_price__c));
				                                             
				                                             
			}
		}
		
		list<Bom_component__c> BomComponentToUpdate1= new list<Bom_component__c>();
		map<Id, list<Extended_Bom_Component__c>> mapBomID_lstExtended = new map<Id, list<Extended_Bom_Component__c>>();
		
		for(Extended_Bom_Component__c objExtended1 : [Select Id, Quantity__c, Unit_price__c ,Bom_Component_Parent__c
		                                            From Extended_Bom_Component__c 
	    	                          				where Bom_Component_Parent__c IN: setBomComponentId ])
	    {
			if(!mapBomID_lstExtended.containsKey(objExtended1.Bom_Component_Parent__c))
	                 mapBomID_lstExtended.put(objExtended1.Bom_Component_Parent__c ,new list<Extended_Bom_Component__c>{objExtended1});
	        else
	                 mapBomID_lstExtended.get(objExtended1.Bom_Component_Parent__c).add(objExtended1);
		
		}
		for(Bom_component__c objBomComponent :[Select a.Name__c,a.Quantity__c,a.Unit_Price__c from Bom_component__c a where id IN:setBomComponentId])
		{
			if(mapBomComponentwithquantity.containsKey(objBomComponent.id))
			{
				objBomComponent.Quantity__c = mapBomComponentwithquantity.get(objBomComponent.id);
				BomComponentToUpdate1.add(objBomComponent);
			}
		}
		
		if(!BomComponentToUpdate1.IsEmpty())
    	{
    		update BomComponentToUpdate1;
    	}
	}
}