<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <tab>standard-Chatter</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Case</tab>
    <tab>standard-Solution</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>department__c</tab>
    <tab>Employee__c</tab>
    <tab>client__c</tab>
    <tab>project_details__c</tab>
    <tab>SALARY_DETAILS__c</tab>
    <tab>detail__c</tab>
    <tab>master__c</tab>
    <tab>Submission__c</tab>
    <tab>Translate__c</tab>
    <tab>Extended_Bom_Component__c</tab>
    <tab>Bom_component__c</tab>
</CustomApplication>
