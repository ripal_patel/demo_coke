trigger ContactDupeCheckTrigger on Contact (before insert) {
 if(TriggerSetting__c.getValues(userInfo.getUserID()).DisableTrigger__c == false){
  CheckDuplicateContacts obj = new CheckDuplicateContacts();
  obj.CheckContacts(trigger.New.get(0)); 
 }
}