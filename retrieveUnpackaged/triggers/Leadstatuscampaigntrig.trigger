trigger Leadstatuscampaigntrig on CampaignMember (before insert) {
 Set<Id> leadid = new Set<Id>();
 Map<Id,String> rsvpdata = new Map<Id,string>();
 for(CampaignMember cm:trigger.new)
 {
  leadid.add(cm.leadID);
  
 }
  for(Lead l1:[select l.Id,l.rsvp__c from Lead l where ID IN :leadid])
 {
  rsvpdata.put(l1.Id,l1.rsvp__c);
 }
 
 for(CampaignMember cmm:trigger.new)
 {
  cmm.rsvp__c = rsvpdata.get(cmm.leadid);
 }
}