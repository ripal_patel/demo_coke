trigger AccountCasesStatusTrigger1 on Case (after insert, after update, after delete)
{
	if(trigger.isinsert && trigger.isAfter)
    {
		AccountCasesStatusHandlerClass1 objCaseHandler = new AccountCasesStatusHandlerClass1();
	    objCaseHandler.afterInsert(trigger.newMap);
    }
    if(trigger.isUpdate && trigger.isAfter)
    {
	    AccountCasesStatusHandlerClass1 objCaseHandler = new AccountCasesStatusHandlerClass1();
	    objCaseHandler.afterUpdate(trigger.newMap , trigger.oldMap);
    }
    if(trigger.isDelete && trigger.isAfter)
    {
    	AccountCasesStatusHandlerClass1 objCaseHandler = new AccountCasesStatusHandlerClass1();
    	objCaseHandler.afterDelete(trigger.oldMap);
    }
}