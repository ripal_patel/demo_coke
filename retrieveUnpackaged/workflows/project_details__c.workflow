<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>X5_days_left</fullName>
        <description>5 days left</description>
        <protected>false</protected>
        <recipients>
            <field>proj_mng_email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/email_notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>proj_mng_mail</fullName>
        <field>proj_mng_email__c</field>
        <formula>Manager__r.Email_Address__c</formula>
        <name>proj mng mail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>status_closed</fullName>
        <field>Status__c</field>
        <literalValue>Closed</literalValue>
        <name>status closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>calender</fullName>
        <actions>
            <name>callen</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>proj notify</fullName>
        <actions>
            <name>proj_mng_mail</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>status closed</fullName>
        <actions>
            <name>status_closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>End_Date__c  =  TODAY()</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>tell mng</fullName>
        <actions>
            <name>X5_days_left</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>TODAY()  + 5= End_Date__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>callen</fullName>
        <assignedTo>ripalpatel52@gmail.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>project_details__c.End_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>callen</subject>
    </tasks>
</Workflow>
