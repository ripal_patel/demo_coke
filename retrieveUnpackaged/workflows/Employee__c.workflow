<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>employee_inactive</fullName>
        <description>employee inactive</description>
        <protected>false</protected>
        <recipients>
            <field>manager_mail__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/employee_deactivated</template>
    </alerts>
    <alerts>
        <fullName>inactive</fullName>
        <description>inactive</description>
        <protected>false</protected>
        <recipients>
            <field>manager_mail__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/employee_deactivated</template>
    </alerts>
    <alerts>
        <fullName>notify</fullName>
        <description>notify</description>
        <protected>false</protected>
        <recipients>
            <field>manager_mail__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/employee_deactivated</template>
    </alerts>
    <alerts>
        <fullName>send_mail_to_mng</fullName>
        <description>send mail to mng</description>
        <protected>false</protected>
        <recipients>
            <field>manager_mail__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/employee_deactivated</template>
    </alerts>
    <fieldUpdates>
        <fullName>mail_mng</fullName>
        <field>Email_Address__c</field>
        <formula>Manager__r.Email_Address__c</formula>
        <name>mail_mng</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>manager_mail</fullName>
        <field>Email_Address__c</field>
        <formula>Manager__r.Email_Address__c</formula>
        <name>manager mail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>mang_mail</fullName>
        <field>manager_mail__c</field>
        <formula>Manager__r.Email_Address__c</formula>
        <name>mang mail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>non_technical</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Non_Technical</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>non technical</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>get mang mail</fullName>
        <actions>
            <name>mang_mail</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>non technical</fullName>
        <actions>
            <name>non_technical</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Employee__c.Current_Employee__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>notify mng</fullName>
        <actions>
            <name>notify</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
