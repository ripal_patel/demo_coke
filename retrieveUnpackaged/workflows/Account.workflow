<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>McaApp__Monthly_Sales_to_Annual_Revenue</fullName>
        <field>AnnualRevenue</field>
        <formula>McaApp__Gross_monthly_Sales__c *12</formula>
        <name>Monthly Sales to Annual Revenue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>McaApp__Update_Record_Type</fullName>
        <description>Update Account Record Type on conversion</description>
        <field>RecordTypeId</field>
        <lookupValue>McaApp__Merchant</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_Owner_Name</fullName>
        <field>Previous_Owner__c</field>
        <formula>IF( CONTAINS(Previous_Owner__c  , &apos;005&apos;) ,  Owner.Username , &apos;&apos; )</formula>
        <name>set Owner Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_id1</fullName>
        <field>Previous_Owner__c</field>
        <formula>IF( PRIORVALUE( Previous_Owner__c )!=null , PRIORVALUE( Previous_Owner1__c ), null)</formula>
        <name>set id1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_id2</fullName>
        <field>Previous2__c</field>
        <formula>IF( PRIORVALUE( Previous_Owner__c )!=null , PRIORVALUE( Previous_Owner1__c ), null)</formula>
        <name>set id2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_owner_id</fullName>
        <field>Previous_Owner__c</field>
        <formula>PRIORVALUE (OwnerId)</formula>
        <name>set owner id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_owner_id152345</fullName>
        <field>Previous_Owner__c</field>
        <formula>Owner.FirstName  +  Owner.LastName</formula>
        <name>set owner id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>McaApp__Account%3ESet Converted Account Record Type</fullName>
        <actions>
            <name>McaApp__Update_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.McaApp__ConvertedAccount__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When Lead is converted, set stage of created Account to &apos;Merchant&apos;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>McaApp__Gross Monthly Sales To Annual Revenue</fullName>
        <actions>
            <name>McaApp__Monthly_Sales_to_Annual_Revenue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>or(and( not(isnull(McaApp__Gross_monthly_Sales__c )),isnull( AnnualRevenue )),and(ischanged(McaApp__Gross_monthly_Sales__c),not(isnull(McaApp__Gross_monthly_Sales__c))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>document_owner</fullName>
        <actions>
            <name>set_owner_id152345</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( OwnerId )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>document_owner1</fullName>
        <actions>
            <name>set_id2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Previous_Owner__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
